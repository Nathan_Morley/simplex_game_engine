#pragma once

#include "Accessor_Concurrent.hpp"

namespace Simplex{
	///<summary>
	///A RAII lock guard for an Accessor_Concurrent.
	///</summary>
	template<typename type_element>
	class Lock_Accessor{
		private:
			Accessor_Concurrent<type_element>& accessor;
		public:
			Lock_Accessor(Accessor_Concurrent<type_element>& accessor);
			~Lock_Accessor();

			type_element& operator*() const;
			type_element& operator->() const;
	};
}

#include "Lock_Accessor.inl"
