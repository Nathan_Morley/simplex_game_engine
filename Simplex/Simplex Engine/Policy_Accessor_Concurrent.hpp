#pragma once

#include "Definitions.hpp"
#include "Pool.hpp"

namespace Simplex{
	template<typename type_element>
	class Policy_Accessor_Concurrent : public Policy_Accessor<type_element, Policy_Accessor_Concurrent<type_element>>{
		protected:
			using policy_pool = Policy_Pool_Concurrent<type_element>;

			friend class Pool<type_element, policy_pool>;

			boolean locked;

			Policy_Accessor_Concurrent(
				Pool<type_element, policy_pool>* _pool_parent,
				index_sentinel_type index_element
			);
			~Policy_Accessor_Concurrent();
		public:
			void lock();
			void unlock();
			void release();
		protected:
			bool isValid() const;
		public:
			type_element& operator*();
			type_element& operator->();
	};
}

#include "Policy_Accessor_Concurrent.inl"
