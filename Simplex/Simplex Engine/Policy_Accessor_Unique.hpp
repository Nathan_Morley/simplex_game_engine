#pragma once

#include "Definitions.hpp"
#include "Policy_Accessor.hpp"
#include "Policy_Pool_Unique.hpp"
#include "Pool.hpp"

namespace Simplex{
	template<typename type_element>
	class Policy_Accessor_Unique : public Policy_Accessor<type_element, Policy_Accessor_Unique<type_element>>{
		protected:
			using policy_pool = Policy_Accessor_Unique<type_element>;

			friend class Pool<type_element, policy_pool>;

			Policy_Accessor_Unique(
				Pool<type_element, policy_pool>* _pool_parent,
				index_sentinel_type index_element
			);
			~Policy_Accessor_Unique();
		public:
			void release();
		protected:
			bool isValid() const;
		public:
			type_element& operator*();
			type_element& operator->();
	};
}

#include "Policy_Accessor_Unique.inl"
