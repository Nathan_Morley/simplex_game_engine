#include <assert.h>

namespace Simplex{
	template<typename type_element>
	inline Policy_Accessor_Unique<type_element>::Policy_Accessor_Unique(
		Pool<type_element, policy_pool>* _pool_parent,
		index_sentinel_type index_element
	):
		Policy_Accessor(static_cast<index_sentinel_type>(index_element)),
		_pool_parent(_pool_parent)
	{
		assert(this -> index_element >= -1);
		assert(this -> _pool_parent != nullptr);
	}

	template<typename type_element>
	inline Policy_Accessor_Unique<type_element>::~Policy_Accessor_Unique(){
		if(isValid()){
			_pool_parent -> release(*this); //RAII element release, perform release() without throwing.
		}
	}

	template<typename type_element>
	inline void Policy_Accessor_Unique<type_element>::release(){
		if(isValid()){
			_pool_parent -> release(*this);
		}else{
			throw new std::runtime_error("Cannot release the element accessor if an element has not been acquired.");
		}
	}

	template<typename type_element>
	inline bool Policy_Accessor_Unique<type_element>::isValid() const{
		return (_pool_parent != nullptr) && (index_element >= 0);
	}

	template<typename type_element>
	inline type_element& Policy_Accessor_Unique<type_element>::operator*(){
		if(!isValid()){
			throw std::runtime_error("Cannot retrieve the element if the element accessor has not been acquired and locked.");
		}

		return _pool_parent -> getElement(index_element);
	}

	template<typename type_element>
	inline type_element& Policy_Accessor_Unique<type_element>::operator->(){
		if(!isValid()){
			throw std::runtime_error("Cannot retrieve the element if the element accessor has not been acquired and locked.");
		}

		return _pool_parent -> getElement(index_element);
	}
}
