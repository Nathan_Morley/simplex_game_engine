#pragma once

namespace Simplex{
	#pragma region template definition
		template<typename type_element>
	#pragma endregion
	class Accessor_Concurrent;
}

#include <atomic>
#include <mutex>
#include <shared_mutex>
#include <variant>

#include "Accessor_Concurrent.hpp"
#include "Definitions.hpp"

namespace Simplex{
	//TODO: Create better summary for pool.

	/*
	Syncronization object documentation:
		There are 3 Pool operations:
			Acquire
				Returns an Accessor_Concurrent if an element is available in the element array.
				Acquire intends to resize if there are no remaining elements in the element array.
				Acquire cannot run at the same time Release or another Acquire is running.
			Release
				An Accessor_Concurrent is returned to the pool to be reused again in the future.
				Release cannot run at the same time Acquire or another Release is running.
				Release has priority over Acquire in order to avoid unnecessary element array resizing.
			Read/Write
				An external operation performed by an Accessor_Concurrent to read from and write to the element contained in the pool.
				A Read/Write operation can be performed at the same time as Acquire, Release and other Read/Write operations, so long as Acquire does not intend to resize.
				If Acquire intends to resize, all new Read/Write operations must wait until Acquire has finished resizing.
	*/

	#pragma region template definition
		template<
			typename type_element_required,
			typename... type_elements_optional
		>
	#pragma endregion
	class Pool_Concurrent{
		private:
			friend class Accessor_Concurrent<type_element_required, type_elements_optional...>;

			std::variant<index_sentinel_type, type_element_required, type_elements_optional...> elements;
			count_type count_active;
			index_sentinel_type index_available_head;
			index_sentinel_type index_available_tail;
			std::atomic_size_t count_read_write; //The amount of read/write (accessor) threads that have locked the pool.
			std::atomic_bool gateClosed; //When an acquire operation intends to resize the element array, it will "close" the gate, blocking new read/write threads from locking.
			std::mutex mutex_manipulate; //The current "manipulating" operation (acquire and release) locks this mutex.
			std::mutex mutex_manipulate_next; //The thread waiting to lock mutex_manipulate will lock this mutex.
			std::mutex mutex_acquire; //Makes acquire operations low priority, release threads can lock mutex_manipulate_next before the next acquire thread can.
			std::mutex mutex_acquire_wait; //Acquire operations that intend to resize the element array will wait for notification on mutex_acquire_wait until all current read/write threads have completed.
			std::shared_mutex mutex_read_write_gate; //When an acquire operation intends to resize the element array, it exclusively locks the mutex_read_write_gate and closes the gate (gateClosed boolean). All new read/write threads will wait on mutex_read_write_gate until the gate is opened.
			std::condition_variable notifier_acquire_wait; //If an acquire operation intends to resize the element array, it locks the mutex_read_write_gate, closes the gate (gateClosed boolean), and then waits for notifications from read/write threads ending.
			std::condition_variable_any notifier_read_write_gate; //If an acquire operation intends to resize the element array, it locks the mutex_read_write_gate and closes the gate (gateClosed boolean), all new threads waiting for the gate to open will wait for a notification from notifier_read_write_gate.
		public:
			Pool_Concurrent(capacity_type capacity_initial);
			Pool_Concurrent(const Pool_Concurrent<type_element_required, type_elements_optional...>&) = delete;
			Pool_Concurrent(Pool_Concurrent<type_element_required, type_elements_optional...>&&) = delete;

			#pragma region template definition
				template<
					typename type_element,
					typename... type_arguments,
					typename std::enable_if<
						is_any_of_v<type_element, type_element_required, type_elements_optional...> &&
						std::is_constructible_v<
							type_element,
							type_arguments...
						>
					>::type* = nullptr
				>
			#pragma endregion
			Accessor_Concurrent<type_element_required, type_elements_optional...> acquire(type_arguments&&... arguments);
			void release(Accessor_Concurrent<type_element_required, type_elements_optional...>& accessor);
			capacity_type capacity() const;
			size_type size() const;
		private:
			#pragma region template definition
				template<
					typename type_element,
					typename std::enable_if<
						is_any_of_v<type_element, type_element_required, type_elements_optional...>
					>::type* = nullptr
				>
			#pragma endregion
			type_element& getElement(index_type index) const;
	};
}

#include "Pool_Concurrent.inl"
