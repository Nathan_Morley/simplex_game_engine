#pragma once

namespace Simplex{
	template<typename type_element>
	class Policy_Accessor_Unique;
}

#include <variant>
#include <vector>

#include "Accessor.hpp"
#include "Definitions.hpp"
#include "Policy_Accessor_Unique.hpp"
#include "Policy_Pool.hpp"

namespace Simplex{
	template<typename type_element>
	class Policy_Pool_Unique : public Policy_Pool{
		protected:
			using policy_accessor = Policy_Accessor_Unique<type_element>;
			using accessor_concurrent = Accessor<type_element, policy_accessor>;
		public:
			std::vector<accessor_concurrent, index_sentinel_type>> elements;
		protected:
			Policy_Pool_Unique(capacity_type capacity_initial);
			#pragma region template definition
				template<
					typename... type_arguments,
					typename = std::enable_if_t<
						std::is_constructible_v<
							type_element,
							type_arguments...
						>
					>
				>
			#pragma endregion
			accessor_concurrent acquire(type_arguments&&... arguments);
			void release(accessor_concurrent& accessor);
		protected:
			void resize();
	};
}

#include "Policy_Pool_Unique.inl"
