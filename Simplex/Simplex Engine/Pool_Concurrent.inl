namespace Simplex{
	#pragma region template definition
		template<
			typename type_element_required,
			typename... type_elements_optional
		>
	#pragma endregion
	inline Pool_Concurrent<type_element_required, type_elements_optional...>::Pool_Concurrent(capacity_type capacity_initial):
	{}

	#pragma region template definition
		template<
			typename type_element_required,
			typename... type_elements_optional
		>
		template<
			typename type_element,
			typename... type_arguments,
			typename std::enable_if<
				is_any_of_v<type_element, type_element_required, type_elements_optional...> &&
				std::is_constructible_v<
					type_element,
					type_arguments...
				>
			>::type*
		>
	#pragma endregion
	Accessor_Concurrent<type_element_required, type_elements_optional...> Pool_Concurrent<type_element_required, type_elements_optional...>::acquire(type_arguments&&... arguments){
	}

	#pragma region template definition
		template<
			typename type_element_required,
			typename... type_elements_optional
		>
	#pragma endregion
	void Pool_Concurrent<type_element_required, type_elements_optional...>::release(Accessor_Concurrent<type_element_required, type_elements_optional...>& accessor){
	}

	#pragma region template definition
		template<
			typename type_element_required,
			typename... type_elements_optional
		>
	#pragma endregion
	inline size_t Pool_Concurrent<type_element_required, type_elements_optional...>::capacity() const{
		return elements.capacity();
	}

	#pragma region template definition
		template<
			typename type_element_required,
			typename... type_elements_optional
		>
	#pragma endregion
	inline size_t Pool_Concurrent<type_element_required, type_elements_optional...>::size() const{
		return capacity() - count_active;
	}

	/*template<typename type_element, typename policy>
	inline void Pool_Concurrent<type_element, policy>::resize(){
	}*/

	#pragma region template definition
		template<
			typename type_element_required,
			typename... type_elements_optional
		>
		template<
			typename type_element,
			typename std::enable_if<
				is_any_of_v<type_element, type_element_required, type_elements_optional...>
			>::type*
		>
	#pragma endregion
	inline type_element& Pool_Concurrent<type_element_required, type_elements_optional...>::getElement(index_type index) const{
		return std::get<type_element>(elements[index]);
	}
}
