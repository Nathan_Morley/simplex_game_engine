#pragma once

//namespace Simplex{
//	class Entity;
//}
//
//#include <unordered_map>
//
//#include "Accessor_Concurrent.hpp"
//#include "Entity.hpp"
//#include "Definitions.hpp"
//#include "Prototype.hpp"
//
//namespace Simplex{
//	class Universe{
//		private:
//			std::unordered_map<id_type, Accessor_Concurrent<Entity>> entities;
//		public:
//			Universe(size_t capacity_initial);
//
//			id_type addEntity();
//			id_type addEntity(Prototype& prototype);
//			void removeEntity(id_type id);
//			Accessor_Concurrent<Entity> getEntityByID(id_type id) const;
//	};
//}
