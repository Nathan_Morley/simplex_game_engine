#pragma once

//namespace Simplex{
//	class Component;
//	class Universe;
//}
//
//#include <memory>
//#include <typeindex>
//#include <unordered_map>
//#include <unordered_set>
//
//#include "Accessor_Concurrent.hpp"
//#include "Component.hpp"
//#include "Definitions.hpp"
//#include "Universe.hpp"
//
//namespace Simplex{
//	class Entity{
//		private:
//			static capacity_type components_capacity;
//			static id_type id_next;
//			bool active;
//			id_type id;
//			id_type id_parent;
//			std::unordered_set<id_type> children;
//			std::unordered_map<
//				std::type_index,
//				std::unique_ptr<Accessor_Concurrent<Component>>
//			> components;
//		public:
//			Entity();
//
//			static void setInitialComponentMapCapacity(capacity_type capacity);
//			id_type getID() const;
//			void setActive(bool active);
//			bool isActive() const;
//			void setParent(
//				id_type _parent,
//				bool isPositionRelative = true
//			);
//			void removeParent();
//			id_type getParent() const;
//			id_type getRoot() const;
//			void setChild(id_type id);
//			void removeChild(id_type id);
//			std::unordered_set<id_type>& getChildren() const;
//			#pragma region template definition
//				template<
//					typename type_component,
//					typename = std::enable_if_t<
//						std::is_base_of_v<
//							Component,
//							type_component
//						>
//					>
//				>
//			#pragma endregion
//			Component& addComponent();
//			#pragma region template definition
//				template<
//					typename type_component,
//					typename = std::enable_if_t<
//						std::is_base_of_v<
//							Component,
//							type_component
//						>
//					>
//				>
//			#pragma endregion
//			void removeComponent();
//			#pragma region template definition
//				template<
//					typename type_component,
//					typename = std::enable_if_t<
//						std::is_base_of_v<
//							Component,
//							type_component
//						>
//					>
//				>
//			#pragma endregion
//			Component& getComponent() const;
//	};
//}
//
//namespace std{
//	template<>
//	class hash<Simplex::Entity>{
//
//		public:
//			size_t operator()(const Simplex::Entity& entity) const{
//				return std::hash<size_t>()(static_cast<size_t>(entity.getID()));
//			}
//	};
//}
