#include <assert.h>

namespace Simplex{
	template<typename type_element>
	inline Policy_Accessor_Concurrent<type_element>::Policy_Accessor_Concurrent(
		Pool<type_element, policy_pool>* _pool_parent,
		index_sentinel_type index_element
	):
		Policy_Accessor(static_cast<index_sentinel_type>(index_element)),
		_pool_parent(_pool_parent),
		locked(false)
	{
		assert(this -> index_element >= -1);
		assert(this -> _pool_parent != nullptr);
	}

	template<typename type_element>
	inline Policy_Accessor_Concurrent<type_element>::~Policy_Accessor_Concurrent(){
		if(isValid()){
			/////////////////////////////////////////////////////////////////////////////////
			//UNLOCKING SHOULD NEVER BE PERFORMED BY THE DESTRUCTOR, USE THE UNLOCK METHOD.//
			/////////////////////////////////////////////////////////////////////////////////
			//if(locked){ //Perform unlock() without throwing.
			//	locked = false;
			//	--(_pool_parent -> count_read_write);

			//	if(_pool_parent -> gateClosed){
			//		_pool_parent -> notifier_acquire_wait.notify_one();
			//	}
			//}

			_pool_parent -> release(*this); //RAII element release, perform release() without throwing.
		}
	}

	template<typename type_element>
	inline void Policy_Accessor_Concurrent<type_element>::lock(){
		if(isValid()){
			if(!isLocked){
				if(_pool_parent -> gateClosed){ //The gate is closed if Pool_Concurrent.acquire() intends to resize the element array.
					std::shared_lock<std::shared_mutex> lock_gate(_pool_parent -> mutex_read_write_gate, std::defer_lock);

					while(_pool_parent -> gateClosed){ //Wait until acquire has completed resizing of the element array.
						_pool_parent -> notifier_read_write_gate.wait(lock_gate);
					}
				}

				++(_pool_parent -> count_read_write);
				isLocked = true;
			}else{
				throw std::runtime_error("The element accessor is already locked.");
			}
		}else{
			throw std::runtime_error("Cannot lock the element accessor if an element has not been acquired.");
		}
	}

	template<typename type_element>
	inline void Policy_Accessor_Concurrent<type_element>::unlock(){
		if(isValid()){
			if(isLocked){
				isLocked = false;
				--(_pool_parent -> count_read_write);

				if(_pool_parent -> isGateClosed){
					_pool_parent -> notifier_acquire_wait.notify_one();
				}
			}else{
				throw std::runtime_error("The element accessor is not locked.");
			}
		}else{
			throw std::runtime_error("Cannot unlock the element accessor if an element has not been acquired and locked.");
		}
	}

	template<typename type_element>
	inline void Policy_Accessor_Concurrent<type_element>::release(){
		if(isValid()){
			if(!locked){
				_pool_parent -> release(*this);
			}else{
				throw std::runtime_error("Cannot release the element accessor if the accessor is locked.");
			}
		}else{
			throw new std::runtime_error("Cannot release the element accessor if an element has not been acquired.");
		}
	}

	template<typename type_element>
	inline bool Policy_Accessor_Concurrent<type_element>::isValid() const{
		return (_pool_parent != nullptr) && (index_element >= 0);
	}

	template<typename type_element>
	inline type_element& Policy_Accessor_Concurrent<type_element>::operator*(){
		if(!isValid()){
			throw std::runtime_error("Cannot retrieve the element if the element accessor has not been acquired and locked.");
		}else if(!isLocked){
			throw std::runtime_error("The element accessor is not locked.");
		}

		return _pool_parent -> getElement(index_element);
	}

	template<typename type_element>
	inline type_element& Policy_Accessor_Concurrent<type_element>::operator->(){
		if(!isValid()){
			throw std::runtime_error("Cannot retrieve the element if the element accessor has not been acquired and locked.");
		}else if(!isLocked){
			throw std::runtime_error("The element accessor is not locked.");
		}

		return _pool_parent -> getElement(index_element);
	}
}
