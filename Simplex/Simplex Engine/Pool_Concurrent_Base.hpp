#pragma once

namespace Simplex{
	#pragma region template definition
		template<typename type_element>
	#pragma endregion
	class Accessor_Concurrent;
}

#include "Accessor_Concurrent.hpp"

namespace Simplex{
	class Pool_Concurrent_Base{
		public:
			template<typename type_element>
			virtual void lockElement(Accessor_Concurrent<type_element>& accessor) = 0;

			template<typename type_element>
			virtual void release(Accessor_Concurrent<type_element>& accessor) = 0;

			template<typename type_element>
			virtual type_element getElement(index_type index_element) = 0;
	};
};
