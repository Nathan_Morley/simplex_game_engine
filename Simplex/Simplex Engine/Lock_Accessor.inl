namespace Simplex{
	template<typename type_element>
	Lock_Accessor<type_element>::Lock_Accessor(Accessor_Concurrent<type_element>& accessor):
		accessor(accessor)
	{
		accessor.lock();
	}

	template<typename type_element>
	Lock_Accessor<type_element>::~Lock_Accessor(){
		accessor.unlock();
	}

	template<typename type_element>
	type_element& Lock_Accessor<type_element>::operator*() const{
		return accessor.operator*();
	}

	template<typename type_element>
	type_element& Lock_Accessor<type_element>::operator->() const{
		return accessor.operator->();
	}
}
