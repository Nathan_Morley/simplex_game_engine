#pragma once

namespace Simplex{
	template<typename type_element>
	class Policy_Accessor_Concurrent;
}

#include <atomic>
#include <mutex>
#include <shared_mutex>
#include <variant>
#include <vector>

#include "Accessor.hpp"
#include "Definitions.hpp"
#include "Policy_Accessor_Concurrent.hpp"
#include "Policy_Pool.hpp"

namespace Simplex{
	template<typename type_element>
	class Policy_Pool_Concurrent : public Policy_Pool{
		protected:
			using policy_accessor = Policy_Accessor_Concurrent<type_element>;
			using accessor_concurrent = Accessor<type_element, policy_accessor>;
		public:
			std::vector<accessor_concurrent, index_sentinel_type>> elements;
			std::atomic_size_t count_read_write; //The amount of read/write (accessor) threads that have locked the pool.
			std::atomic_bool gateClosed; //When an acquire operation intends to resize the element array, it will "close" the gate, blocking new read/write threads from locking.
			std::mutex mutex_manipulate; //The current "manipulating" operation (acquire and release) locks this mutex.
			std::mutex mutex_manipulate_next; //The thread waiting to lock mutex_manipulate will lock this mutex.
			std::mutex mutex_acquire; //Makes acquire operations low priority, release threads can lock mutex_manipulate_next before the next acquire thread can.
			std::mutex mutex_acquire_wait; //Acquire operations that intend to resize the element array will wait for notification on mutex_acquire_wait until all current read/write threads have completed.
			std::shared_mutex mutex_read_write_gate; //When an acquire operation intends to resize the element array, it exclusively locks the mutex_read_write_gate and closes the gate (gateClosed boolean). All new read/write threads will wait on mutex_read_write_gate until the gate is opened.
			std::condition_variable notifier_acquire_wait; //If an acquire operation intends to resize the element array, it locks the mutex_read_write_gate, closes the gate (gateClosed boolean), and then waits for notifications from read/write threads ending.
			std::condition_variable_any notifier_read_write_gate; //If an acquire operation inteds to resize the element array, it locks the mutex_read_write_gate and closes the gate (gateClosed boolean), all new threads waiting for the gate to open will wait for a notification from notifier_read_write_gate.
		protected:
			Policy_Accessor_Concurrent(capacity_type capacity_initial);
		public:
			#pragma region template definition
				template<
					typename... type_arguments,
					typename = std::enable_if_t<
						std::is_constructible_v<
							type_element,
							type_arguments...
						>
					>
				>
			#pragma endregion
			accessor_concurrent acquire(type_arguments&&... arguments);
			void release(accessor_concurrent& accessor);
		protected:
			void resize();
	};
}

#include "Policy_Pool_Concurrent.inl"
