#pragma once

//namespace Simplex{
//	class Entity;
//}
//
//#include "Definitions.hpp"
//#include "Entity.hpp"
//
//namespace Simplex{
//	enum Flags_Component{
//		ACTIVE,
//		DIRTY,
//		DESTROY,
//		_flag_set_size
//	};
//
//	class Component{
//		protected:
//			id_type id_entity_parent;
//			bool active;
//
//			Component(id_type id_entity_parent);
//		public:
//			virtual void initialize() = 0; //Perform one-time calculations after construction.
//			virtual void update(time_difference time_delta) = 0; //Perform calculations every update interval if the component is active.
//			
//			void setActive(bool active);
//			bool isActive() const;
//			void setParentEntity(id_type id_entity_parent);
//			id_type getParentEntity() const;
//			id_type getRootParentEntity() const;
//
//			virtual void onUpdateLast() = 0; //Perform one-time calculations before destruction (NOT A REPLACEMENT FOR THE DESTRUCTOR).
//			virtual void onSetAsParent(id_type id_entity_child) = 0;
//			virtual void onUnsetAsParent(id_type id_entity_child) = 0;
//			virtual void onSetAsChild(id_type id_entity_parent) = 0;
//			virtual void onUnsetAsChild(id_type id_entity_parent) = 0;
//			virtual void onComponentAdded(type_info) = 0;
//			virtual void onComponentRemoved() = 0;
//			virtual void onComponentAdded_superEntity(id_type id_entity_parent) = 0;
//			virtual void onComponentRemoved_superEntity(id_type id_entity_parent) = 0;
//			virtual void onComponentAdded_childEntity(id_type id_entity_child) = 0;
//			virtual void onComponentRemoved_childEntity(id_type id_entity_child) = 0;
//			//onCollision
//			//onFamilyCollision
//	};
//}
//
//#include "Component.inl"
