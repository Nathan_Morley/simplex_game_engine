#pragma once

#include "Definitions.hpp"
#include "Pool_Concurrent.hpp"

namespace Simplex{
	#pragma region template definition
		template<typename type_element>
	#pragma endregion
	class Accessor_Concurrent{
		private:
			friend class Pool_Concurrent<type_element_required, type_elements_optional...>;

			Pool_Concurrent<type_element_required, type_elements_optional...>* _pool_parent;
			index_type index_element;
			boolean locked = false;
		public:
			Accessor_Concurrent(
				Pool_Concurrent<type_element_required, type_elements_optional...>* _pool_parent,
				index_type index_element
			);
			Accessor_Concurrent(Accessor_Concurrent const&) = delete;
			Accessor_Concurrent(Accessor_Concurrent&&) = delete;

			void lock();
			void unlock();
			void release();
			#pragma region template definition
				template<
					typename type_element,
					typename std::enable_if<
						is_any_of<type_element, type_element_required, type_elements_optional...>
					>::type* = nullptr
				>
			#pragma endregion
			type_element& get();
	};
}

#include "Accessor_Concurrent.inl"
