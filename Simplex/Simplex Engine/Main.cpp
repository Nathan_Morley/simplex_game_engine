#include <iostream>

#include "Component.hpp"
#include "Definitions.hpp"
#include "Entity.hpp"
#include "Lock_Accessor.hpp"
#include "Pool_Concurrent.hpp"
#include "Universe.hpp"

struct foo{
	private:
		Simplex::integer8_unsigned value;
	public:
		Simplex::integer8_unsigned& operator*(){
			return value;
		}

		void operator++(){
			++value;
		}

		void operator++(int){
			value++;
		}
};

void main(){
	Simplex::Pool_Concurrent<foo> pool_foo(3);

	//auto Accessor_Concurrent = pool_foo.acquire();

	std::cout << "Hello World." << std::endl;
}
