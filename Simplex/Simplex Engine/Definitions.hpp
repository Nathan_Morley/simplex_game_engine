#pragma once

#include <cstddef>
#include <type_traits>

#include "glm\vec2.hpp"
#include "glm\vec3.hpp"
#include "glm\vec4.hpp"
#include "glm\mat3x3.hpp"
#include "glm\mat4x4.hpp"
#include "glm\gtc\quaternion.hpp"
#include "type_safe\bounded_type.hpp"
#include "type_safe\constrained_type.hpp"
//#include "type_safe\flag_set.hpp"
#include "type_safe\index.hpp"
#include "type_safe\types.hpp"

namespace Simplex{
	/*
	#pragma region arbitrary types
		typedef type_safe::ptrdiff_t pointer_difference;

		typedef type_safe::bool_t boolean;

		typedef type_safe::int8_t byte;
		//typedef type_safe::int8_t integer8;
		typedef type_safe::int8_t integer8_signed;
		typedef type_safe::uint8_t integer8_unsigned;

		//typedef type_safe::int16_t integer16;
		typedef type_safe::int16_t integer16_signed;
		typedef type_safe::uint16_t integer16_unsigned;

		//typedef type_safe::int32_t integer32;
		typedef type_safe::int32_t integer32_signed;
		typedef type_safe::uint32_t integer32_unsigned;

		//typedef type_safe::int64_t integer64;
		typedef type_safe::int64_t integer64_signed;
		typedef type_safe::uint64_t integer64_unsigned;
		
		typedef type_safe::size_t size_type; //Represents a size, usually the size of a container or collection.
		typedef type_safe::size_t capacity_type; //Represents a capacity, usually the capacity of a container or collection.

		typedef type_safe::size_t id_type; //Represents an identifier.

		typedef type_safe::float_t float_type;
		typedef type_safe::float_t real_number;
		
		typedef type_safe::double_t double_type;

		typedef type_safe::index_t index_type; //Represents an index, usually the index of an element in a container or collection.

		#pragma region template definition
			template<typename enumeration>
		#pragma endregion
		using flag_set = type_safe::flag_set<enumeration>;
		#pragma region template definition
			template<typename enumeration>
		#pragma endregion
		using flag_combo = type_safe::flag_combo<enumeration>;
		#pragma region template definition
			template<typename enumeration>
		#pragma endregion
		using flag_mask = type_safe::flag_mask<enumeration>;
	#pragma endregion
	#pragma region time types
		//type_safe::floating_point<float>
		typedef type_safe::floating_point<float> time_difference; //Represents (time - time), or in other words, "elapsed time."
		typedef type_safe::floating_point<float> time_accumulation; //Represents (time_accumulation += time_difference), or in other words, the sum of multiple elapsed time measurements.
	#pragma endregion
	*/

	#pragma region SFINAE
		#pragma region is_any_of
			#pragma region template definition
				template<
					class Type,
					class...
				>
			#pragma endregion
			struct is_any_of : std::false_type{}; //Recursion base case

			#pragma region template definition
				template<
					class Type,
					class Head,
					class... Tail
				>
			#pragma endregion
			struct is_any_of<Type, Head, Tail...> : std::conditional_t<
				std::is_same_v<Type, Head>,
				std::true_type,
				is_any_of<Type, Tail...>
			>{};

			#pragma region template definition
				template<
					class Type,
					class Head,
					class... Tail
				>
			#pragma endregion
			constexpr bool is_any_of_v = is_any_of<Type, Head, Tail>::value;
		#pragma endregion
	#pragma endregion
	#pragma region constants
		#pragma region template definition
			template<
				typename type = float_t,
				typename = std::enable_if_t<
					std::is_floating_point_v<type>
				>
			>
		#pragma endregion
		constexpr type pi = type(3.1415926535897932385L);

		#pragma region template definition
			template<
				typename type = float_t,
				typename = std::enable_if_t<
					std::is_floating_point_v<type>
				>
			>
		#pragma endregion
		constexpr type pi_two = type(3.1415926535897932385L * 2.0L);

		#pragma region template definition
			template<
				typename type = float_t,
				typename = std::enable_if_t<
					std::is_floating_point_v<type>
				>
			>
		#pragma endregion
		constexpr type pi_half = type(3.1415926535897932385L / 2.0L);

		#pragma region template definition
			template<
				typename type = float_t,
				typename = std::enable_if_t<
					std::is_floating_point_v<type>
				>
			>
		#pragma endregion
		constexpr type pi_quarter = type(3.1415926535897932385L / 4.0L);

		#pragma region template definition
			template<
				typename type = float_t,
				typename = std::enable_if_t<
					std::is_floating_point_v<type>
				>
			>
		#pragma endregion
		constexpr type phi = type(1.6180339887498948482L);

		#pragma region template definition
			template<
				typename type = float_t,
				typename = std::enable_if_t<
					std::is_floating_point_v<type>
				>
			>
		#pragma endregion
		constexpr type golden_ratio = phi<type>;

		#pragma region template definition
			template<
				typename type = float_t,
				typename = std::enable_if_t<
					std::is_floating_point_v<type>
				>
			>
		#pragma endregion
		constexpr type light_speed = type(2.99792458e8L);

		#pragma region template definition
			template<
				typename type = float_t,
				typename = std::enable_if_t<
					std::is_floating_point_v<type>
				>
			>
		#pragma endregion
		constexpr type gravitational_constant = type(6.67384e-11L);
	#pragma endregion
	#pragma region descriptive fundamental types
		typedef type_safe::boolean boolean;

		template<
			typename type,
			typename = std::enable_if_t<
				std::is_unsigned_v<type> &&
				std::is_same_v<std::remove_cv_t<type>, bool>
			>
		>
		using index_sentinel_type = type_safe::constraints::greater_equal<type, std::integral_constant<type, -1>>;

		typedef type_safe::int8_t integer8_signed;
		typedef type_safe::uint8_t integer8_unsigned;
		typedef type_safe::uint8_t size8_type;
		typedef type_safe::uint8_t capacity8_type;
		typedef type_safe::uint8_t count8_type;
		typedef type_safe::uint8_t index8_type;
		//type_safe::bounded_type<int8_t, true, true, type_safe::constraints::greater_equal<int8_t, -1_bound> index8_sentinel_type;
		//typedef type_safe::constrained_type<
		//	type_safe::int8_t,
		//	type_safe::constraints::bounded<
		//		type_safe::int8_t,
		//		true,
		//		true,
		//		//What goes here
		//	>
		//> index8_sentinel_type;
		
		typedef index_sentinel_type<type_safe::uint8_t> index8_sentinel_type; // WIP ////////////////////////////////////

		typedef int16_t integer16_signed;
		typedef uint16_t integer16_unsigned;
		typedef uint16_t size16_type;
		typedef uint16_t capacity16_type;
		typedef uint16_t count16_type;
		typedef uint16_t index16_type;
		typedef int16_t index16_sentinel_type;

		typedef int32_t integer32_signed;
		typedef uint32_t integer32_unsigned;
		typedef uint32_t size32_type;
		typedef uint32_t capacity32_type;
		typedef uint32_t count32_type;
		typedef uint32_t index32_type;
		typedef int32_t index32_sentinel_type;

		typedef int64_t integer64_signed;
		typedef uint64_t integer64_unsigned;
		typedef uint64_t size64_type;
		typedef uint64_t capacity64_type;
		typedef uint64_t count64_type;
		typedef uint64_t index64_type;
		typedef int64_t index64_sentinel_type;

		typedef ptrdiff_t pointer_difference;

		typedef std::size_t size_type; //Represents a size, usually the size of a container or collection.
		typedef std::size_t capacity_type; //Represents a capacity, usually the capacity of a container or collection.
		typedef std::size_t count_type;
		typedef std::size_t index_type;
		//typedef signed long long index_sentinel_type; //Represents an index, but has the added ability to store a sentinel (special) value.

		typedef uint32_t id_type; //Represents an identifier.

		typedef float_t float_type;
	
		typedef double_t double_type;

		typedef long double long_double_type;

		#pragma region template definition
			template<
				typename type = float_t,
				typename = std::enable_if_t<
					std::is_floating_point_v<type>
				>
			>
		#pragma endregion
		using real_number = type;

		#pragma region template definition
			template<
				typename type,
				typename = std::enable_if_t<
					std::is_floating_point_v<type>
				>
			>
		#pragma endregion
		using time_difference = type;
		#pragma region template definition
			template<
				typename type,
				typename = std::enable_if_t<
					std::is_floating_point_v<type>
				>
			>
		#pragma endregion
		using time_accumulation = type;
	#pragma endregion
	#pragma region glm::vec2 types
		typedef glm::vec2 vector2; //A non-specific 2-dimensional vector type, slightly more descriptive than "vec2."
		typedef glm::vec2 vector2_difference; //Represents (vector2 - vector2).

		typedef glm::vec2 point2; //Represents an "object" that consists only of a 2-dimensional position in space.
		typedef glm::vec2 coordinate2; //Represents a 2-dimensional coordinate on a surface.

		typedef glm::vec2 position2; //Represents a 2-dimensional position.
		typedef glm::vec2 position2_difference; //Represents (position2 - position2).
		typedef glm::vec2 velocity2; //Represents the first derivative of a 2-dimensional position.
		typedef glm::vec2 acceleration2; //Represents the second derivative of a 2-dimensional position.
		typedef glm::vec2 force2; //Represents a 2-dimensional force vector.

		typedef glm::vec2 direction2; //Represents a 2-dimensional direction.
		typedef glm::vec2 ray2; //Represents a position2 + direction2.
	#pragma endregion
	#pragma region glm::vec3 types
		typedef glm::vec3 vector3; //A non-specific 3-dimensional vector type, slightly more descriptive than "vec3."
		typedef glm::vec3 vector3_difference; //Represents (vector3 - vector3).

		typedef glm::vec3 point3; //Represents an "object" that consists only of a 3-dimensional position in space.

		typedef glm::vec3 position3; //Represents a 3-dimensional position.
		typedef glm::vec3 position3_difference; //Represents (position3 - position3).
		typedef glm::vec3 velocity3; //Represents the first derivative of a 3-dimensional position.
		typedef glm::vec3 acceleration3; //Represents the second derivative of a 3-dimensional position.
		typedef glm::vec3 force3; //Represents a 3-dimensional force vector.

		typedef glm::vec3 direction3; //Represents a 3-dimensional direction.
		typedef glm::vec3 ray3; //Represents a position3 + direction3.

		typedef glm::vec3 angle3; //Represents a 3-dimensional Euler angle.
		typedef glm::vec3 angularVelocity3; //Represents the first derivative of a 3-dimensional angle.
		typedef glm::vec3 angularAcceleration3; //Represents the second derivative of a 3-dimensional angle.
		typedef glm::vec3 torque3; //Represents a 3-dimensional torque, or "angular force".
	#pragma endregion
	#pragma region glm::vec4 types
		typedef glm::vec4 vector4; //A non-specific 4-dimensional vector type, slightly more descriptive than "vec4."
	#pragma endregion
	#pragma region glm::mat3x3 types
		typedef glm::mat3x3 matrix3; //A non-specific matrix4 type, slightly more descriptive than "mat4x4."
		typedef glm::mat3x3 transform3; //Represents a transformation matrix, the product of multiplying matrices.
		typedef glm::mat3x3 rotation3; //Represents a matrix that rotates the basis by an angle or orientation.
		typedef glm::mat3x3 scale3; //Represents a matrix that scales the basis by some scale values.
	#pragma endregion
	#pragma region glm::mat4x4 types
		typedef glm::mat4x4 matrix4; //A non-specific matrix4 type, slightly more descriptive than "mat4x4."
		typedef glm::mat4x4 transform4; //Represents a transformation matrix, the product of multiplying matrices.
		typedef glm::mat4x4 translation4; //Represents a matrix that offsets the basis by a position.
		typedef glm::mat4x4 rotation4; //Represents a matrix that rotates the basis by an angle or orientation.
		typedef glm::mat4x4 scale4; //Represents a matrix that scales the basis by some scale values.
		typedef glm::mat4x4 perspective4; //Represents a matrix that transforms the basis from a frustum to a normalized cube.
	#pragma endregion
	#pragma region glm::quat types
		typedef glm::quat quaternion; //A non-specific quaternion type, slightly more descriptive than "quat."
	#pragma endregion
}
