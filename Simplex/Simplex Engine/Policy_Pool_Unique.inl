namespace Simplex{
	#pragma region template definition
		template<typename type_element>
	#pragma endregion
	Policy_Pool_Unique<type_element>::Policy_Pool_Unique(capacity_type capacity_initial):
		Policy_Pool(capacity_initial),
	{
		elements.reserve(capacity_initial);
	}

	#pragma region template definition
		template<typename type_element>
		template<
			typename... type_arguments,
			typename enable_if
		>
	#pragma endregion
	inline Policy_Pool_Unique<type_element>::accessor_concurrent Policy_Pool_Unique<type_element>::acquire(type_arguments&&... arguments){
		return accessor_concurrent();
	}

	#pragma region template definition
		template<typename type_element>
	#pragma endregion
	inline void Policy_Pool_Unique<type_element>::release(accessor_concurrent& accessor){

	}

	#pragma region template definition
		template<typename type_element>
	#pragma endregion
	inline void Policy_Pool_Unique<type_element>::resize(){

	}
}
