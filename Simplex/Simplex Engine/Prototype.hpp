#pragma once

//#include <unordered_set>
//
//#include "Component.hpp"
//#include "Entity.hpp"
//
//namespace Simplex{
//	class Prototype{
//		private:
//			std::unordered_set<std::type_index> component_types;
//			size_t count_components;
//		public:
//			Prototype(size_t capacity_initial);
//			#pragma region template definition
//				template<
//					typename... type_arguments,
//					typename = std::enable_if_t<
//						std::is_base_of<
//							std::type_index,
//							type_arguments...
//						>::value
//					>
//				>
//			#pragma endregion
//			Prototype(type_arguments&&... componentTypes);
//
//			void addComponent(std::type_index componentType);
//			#pragma region template definition
//				template<
//					typename... type_arguments,
//					typename = std::enable_if_t<
//						std::is_base_of<
//							std::type_index,
//							type_arguments...
//						>::value
//					>
//				>
//			#pragma endregion
//			void addComponents(type_arguments&&... componentTypes);
//			std::unordered_set<std::type_index>& getComponents() const;
//			size_t getComponentCount() const;
//	};
//}
