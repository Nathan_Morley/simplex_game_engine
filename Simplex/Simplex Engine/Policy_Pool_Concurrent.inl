namespace Simplex{
	#pragma region template definition
		template<typename type_element>
	#pragma endregion
	Policy_Pool_Concurrent<type_element>::Policy_Pool_Concurrent(capacity_type capacity_initial):
		Policy_Pool(capacity_initial),
		count_read_write(0),
		gateClosed(false)
	{
		elements.reserve(capacity_initial);
	}

	#pragma region template definition
		template<typename type_element>
		template<
			typename... type_arguments,
			typename = std::enable_if_t<
				std::is_constructible_v<
					type_element,
					type_arguments...
				>
			>
		>
	#pragma endregion
	inline Policy_Pool_Concurrent<type_element>::accessor_concurrent Policy_Pool_Concurrent<type_element>::acquire(type_arguments&&... arguments){
		return accessor_concurrent();
	}

	#pragma region template definition
		template<typename type_element>
	#pragma endregion
	inline void Policy_Pool_Concurrent<type_element>::release(accessor_concurrent& accessor){

	}

	#pragma region template definition
		template<typename type_element>
	#pragma endregion
	inline void Policy_Pool_Concurrent<type_element>::resize(){

	}
}
